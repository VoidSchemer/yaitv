/* 
 * Copyright (C) 2015 Balushkin M.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var express = require('express');
var path = require('path');
var http = require('superagent');

var app = express();

// Директории со "статичными" ресурсами
app.use('/css', express.static(path.join(__dirname, 'css')));
app.use('/lib', express.static(path.join(__dirname, 'lib')));
app.use('/app', express.static(path.join(__dirname, 'app')));
app.use('/fonts', express.static(path.join(__dirname, 'fonts')));

// index.html, корень сайта
app.get(
  '/',
  function (req, res) {
    console.log('index');
    res.sendFile(path.join(__dirname + '/index.html'));
  }
);

// Поиск
app.get(
  '/api/search/:entity/:term',
  function (req, res) {
    console.log(req.params.term + ' ' + req.params.entity);
    var term = req.params.term;
    var entity = req.params.entity;
    
    http.get('https://itunes.apple.com/search?term=' + term + '&entity=' + entity)
      .end(function (err, data) { res.send(data.text); });
  }
);

// Просмотр
app.get(
  '/api/lookup/:entity/:id',
  function (req, res) {
    var entity = req.params.entity;
    var id = req.params.id;
    
    http.get('https://itunes.apple.com/lookup?id=' + id + '&entity=' + entity)
      .end(function (err, data) { res.send(data.text); });
  }
);

// Запуск сервера
app.listen(3000);
console.log('Running!');