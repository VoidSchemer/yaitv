/* 
 * Copyright (C) 2015 Balushkin M.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

app.controller('albumController', [
  '$scope', '$routeParams', 'itunesApi',
  function ($scope, $routeParams, itunesApi) {
    var artistId = $routeParams.artistId;
    var albumId = $routeParams.albumId;
    
    itunesApi.lookupSongs(albumId)
      .then(function (data) {
      $scope.album = data.data.results[0];
      data.data.results.shift();
      $scope.songs = data.data.results;
    });
  }
]);