/* 
 * Copyright (C) 2015 Balushkin M.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var app = angular.module('itunesPlayer', ['ngRoute']);

app.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when('/', { templateUrl: 'app/views/indexView.html', controller: 'indexController' });
    $routeProvider.when('/artist/:id', { templateUrl: 'app/views/artistView.html', controller: 'artistController' });
    $routeProvider.when('/artist/:artistId/album/:albumId', 
      { templateUrl: 'app/views/albumView.html', controller: 'albumController' }
    );
    
    $routeProvider.otherwise('/');
  }
]);