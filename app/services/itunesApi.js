/* 
 * Copyright (C) 2015 Balushkin M.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

app.factory('itunesApi', [ 
  '$http',
  function ($http) {
    var urlBase = 'http://localhost:3000/api';
    var itunesApi = {};

    itunesApi.search = function (term, entity) {
      return $http.get(urlBase + '/search/' + entity + '/' + term);
    };
    itunesApi.searchArtist = function (name) {
      return itunesApi.search(name, 'musicArtist');
    };
    itunesApi.lookup = function (id, entity) {
      return $http.get(urlBase + '/lookup/' + entity + '/' + id);
    };
    itunesApi.lookupAlbums = function (artist) {
      return itunesApi.lookup(artist, 'album');
    };
    itunesApi.lookupSongs = function (album) {
      return itunesApi.lookup(album, 'song');
    };
    
    return itunesApi;
}]);